sbtPlugin := true

organization := "com.ignitesales.sbt.plugins"

name := "sbt-yui-compressor"

version := "2.0.0"

description := "sbt-yui-compressor is an SBT plugin for YUI Compressor to minify CSS and JavaScript."

licenses += ("Apache License, Version 2.0", url("http://www.apache.org/licenses/LICENSE-2.0.txt"))

scalacOptions ++= DefaultOptions.scalac :+ Opts.compile.deprecation

libraryDependencies += "com.yahoo.platform.yui" % "yuicompressor" % "2.4.8"
