package com.ignitesales.sbt.plugin.yuiCompressor

import sbt.{Def, _}
import Keys._
import Path.{flat, rebase}

/**
  * CSS and JavaScript compressor for SBT using YUI Compressor.
  */
object YuiCompressor extends AutoPlugin {

  object autoImport {
    object YuiCompressorKeys extends YuiCompressorKeys
  }

  private val yui = YuiCompressorKeys

  private def compressorTask(resources: TaskKey[Seq[File]], resourceDirs: SettingKey[Seq[File]], task: TaskKey[Seq[File]]) = Def.taskDyn {
    val s = (streams in task).value
    val cacheDir = s.cacheDirectory
    val in = resources.value
    val outdir = (resourceManaged in task).value
    val dirs = resourceDirs.value
    val suf = (yui.minSuffix in task).value
    val state0 = (state in task).value
    val runner0 = (runner in task).value
    val opts = (yui.options in task).value
    val log0 = s.log
    Def.task {
      compressorTask0(
        cacheDir = cacheDir,
        in = in,
        outdir = outdir,
        dirs = dirs,
        suffix = suf,
        state = state0,
        runner = runner0,
        options = opts,
        log = log0
      )
    }
  }

  private def compressorTask0(cacheDir: File, in: Seq[File], outdir: File, dirs: Seq[File], suffix: String, state: State, runner: ScalaRun, options: Seq[String], log: Logger) = {
    def appendSuffix(file: File, suffix: String): File = file.getParentFile / (file.base + suffix + "." + file.ext)
    val mappings = (in --- dirs) pair (rebase(dirs, outdir) | flat(outdir)) map { pair => (pair._1, appendSuffix(pair._2, suffix)) }
    Compressor(cacheDir, mappings, Project.extract(state).currentUnit.unit.plugins.classpath, runner, options, log)
  }

  private def yuiCollectFiles(key: TaskKey[Seq[File]]) =
    Defaults.collectFiles(unmanagedResourceDirectories in key, includeFilter in key, excludeFilter in key)

  private def generatorConfigCommon(key: TaskKey[Seq[File]]) =
    inTask(key) {
      Seq(
        Defaults.runnerTask
      )
    }

  lazy val yuiBaseSettings: Seq[Setting[_]] =
    Seq(
      yui.minSuffix := (yui.minSuffix ?? "-min").value,
      yui.options   := (yui.options ?? Nil).value,
      includeFilter in yui.cssResources := "*.css",
      includeFilter in yui.jsResources  := "*.js",
      excludeFilter in yui.cssResources := (excludeFilter in unmanagedResources).value,
      excludeFilter in yui.jsResources  := (excludeFilter in unmanagedResources).value,
      unmanagedResourceDirectories in yui.cssResources := unmanagedResourceDirectories.value,
      unmanagedResourceDirectories in yui.jsResources  := unmanagedResourceDirectories.value)

  lazy val yuiResourceConfig: Seq[Setting[_]] =
    Seq(
      yui.cssResources := yuiCollectFiles(yui.cssResources).value,
      yui.jsResources  := yuiCollectFiles(yui.jsResources).value,
      watchSources in Defaults.ConfigGlobal ++= yui.cssResources.value ++ yui.jsResources.value)

  lazy val yuiGeneratorConfig: Seq[Setting[_]] =
    generatorConfigCommon(yui.cssCompressor) ++
    generatorConfigCommon(yui.jsCompressor) ++
    Seq(
      yui.options in yui.cssCompressor ++= Seq("--type", "css"),
      yui.options in yui.jsCompressor  ++= Seq("--type", "js"),
      yui.cssCompressor   := compressorTask(yui.cssResources, unmanagedResourceDirectories in yui.cssResources, yui.cssCompressor).value,
      yui.jsCompressor    := compressorTask(yui.jsResources, unmanagedResourceDirectories in yui.jsResources, yui.jsCompressor).value,
      resourceGenerators ++= yui.cssCompressor.taskValue :: yui.jsCompressor.taskValue :: Nil)

  lazy val yuiCompressorConfigs: Seq[Setting[_]] = yuiBaseSettings ++ yuiResourceConfig ++ yuiGeneratorConfig

  lazy val yuiSettings: Seq[Setting[_]] = inConfig(Compile)(yuiCompressorConfigs) ++ inConfig(Test)(yuiCompressorConfigs)

  override lazy val projectSettings: Seq[Setting[_]] = yuiSettings

}
