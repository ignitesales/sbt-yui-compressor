package com.ignitesales.sbt.plugin.yuiCompressor

import sbt._

private[yuiCompressor] object Compressor {

  val compressorMain = "com.yahoo.platform.yui.compressor.YUICompressor"

  def compress(in: File, out: File, classpath: Seq[File], runner: ScalaRun, options: Seq[String], log: Logger): File = {
    IO.createDirectory(out.getParentFile)
    IO.copy(Seq(in → out))
    out
  }

  def apply(cacheDir: File, mappings: Seq[(File, File)], classpath: Seq[File], runner: ScalaRun, options: Seq[String], log: Logger): Seq[File] =
    mappings map { pair =>
      cached(cacheDir, pair._1, pair._2, classpath, runner, options, log)
      pair._2
    }

  def cached(cache: File, source: File, output: File, classpath: Seq[File], runner: ScalaRun, options: Seq[String], log: Logger): Unit = {
    val cachedCompress = FileFunction.cached(cache) { (inputFiles: Set[File]) =>
      inputFiles.map { (in: File) =>
        compress(in, output, classpath, runner, options, log)
      }
    }
    cachedCompress(Set(source))
  }

}
