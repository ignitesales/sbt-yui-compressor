package com.ignitesales.sbt.plugin.yuiCompressor

/**
  * Convenient helper for YUI Compressor options.
  */
private[yuiCompressor] object Opts {
  def charset(c: String)  = Seq("-charset", c)
  def lineBreak(col: Int) = Seq("--line-break", col)
  val verbose             = "--verbose"
  object js {
    val nomunge              = "--nomunge"                 
    val preserveSemi         = "--preserve-semi" 
    val disableOptimizations = "--disable-optimizations "
  }
}

private[yuiCompressor] object DefaultOptions {
	import Opts._
	def cssCompressor: Seq[String] = charset("UTF-8")
  def jsCompressor: Seq[String]  = charset("UTF-8")
}
