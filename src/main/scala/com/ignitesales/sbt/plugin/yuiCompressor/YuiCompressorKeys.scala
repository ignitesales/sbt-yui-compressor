package com.ignitesales.sbt.plugin.yuiCompressor

import sbt.{File, SettingKey, TaskKey}

trait YuiCompressorKeys {
  lazy val minSuffix     = SettingKey[String]("yui-min-suffix", "Suffix of the base of the minified files.")
  lazy val options       = SettingKey[Seq[String]]("yui-options", "YUI Compressor options.")
  lazy val cssResources  = TaskKey[Seq[File]]("yui-css-resources", "CSS resources to be minified.")
  lazy val jsResources   = TaskKey[Seq[File]]("yui-js-resources", "JavaScript resources to be minified.")
  lazy val cssCompressor = TaskKey[Seq[File]]("yui-css-compressor", "CSS compressor task.")
  lazy val jsCompressor  = TaskKey[Seq[File]]("yui-js-compressor", "JavaScript compressor task.")
}

object YuiCompressorKeys extends YuiCompressorKeys
