isSnapshot := version.value.trim.endsWith("SNAPSHOT")

publishTo := {
  val artifactory = "https://ignitesales.jfrog.io/artifactory/"
  if (isSnapshot.value) {
    Some("ignite-artifactory-snapshots" at s"${artifactory}ignitesales-sbt-release-local;build.timestamp=" + new java.util.Date().getTime)
  } else {
    Some("ignite-artifactory-releases" at s"${artifactory}ignitesales-sbt-release-local")
  }
}

credentials += Credentials("Artifactory Realm", "ignitesales.jfrog.io", "jjames@ignitesales.com", "APAJ7kgV5QP9xWU3TT3Af7dP4Ey")
//credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

//change to true if publishing to cloud artifactory, keep false for local
publishMavenStyle := false
